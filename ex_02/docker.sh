#!/bin/bash
# Comandos para gerar o exercicio

echo "Criando volumepara uso dos containers de nginx"
docker volume create vol_compartilhado

echo "Criando container 01 nginx"
docker run -d -p 80:80 --name ct_nginx_01 -v vol_compartilhado:/usr/share/nginx/html nginx:latest

echo "Criando container 02 nginx"
docker run -d -p 81:80 --name ct_nginx_02 -v vol_compartilhado:/usr/share/nginx/html nginx:latest

echo "Criando container nano - considerando que a imagem ja tenha sido criada"
docker run --name ct_nano -ti -v vol_compartilhado:/usr_files withnano

echo "Acessando container nano"
docker exec -t1 ct_nano /bin/bash


echo "criando arquivo teste.html no volume pelo container nano"

cd usr_files
nano teste.html

<!DOCTYPE html>
<html>
<body>

<h1>Teste Docker</h1>
<p>Criando arquivos em container.</p>

</body>
</html>

^O
^X

exit